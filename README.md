Working solution:

```
mergetomaster:
  stage: merge
  image: alpine
  before_script:
    - apk add --update git
    - git config user.email "it@geg.it"
    - git config user.name "IT GEG"
    - git fetch origin
  script:
    - git checkout master
    - git merge ${CI_COMMIT_REF_NAME}
    - git push https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN_VALUE}@gitlab.com/${CI_PROJECT_PATH} master
  rules:
    - if: '$CI_COMMIT_TAG != null'
```
